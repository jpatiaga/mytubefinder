define [
  'jquery'
  'underscore'
  'backbone'
  'collections/videos/VideosCollection'
  'views/videos/VideosListView'
  'text!templates/videos/videosTemplate.html'
], ($, _, Backbone, VideosCollection, VideosListView, videosTemplate) ->

  HomeView = Backbone.View.extend

    el: $('#content')

    events:
      'click #search': 'search'
      'submit form': 'search'

    initialize: ->
      that = this

      onDataHandler = (collection) ->
        that.render()

      if localStorage.mytubeFinder_search_term
        that.collection = new VideosCollection [],
        search_term: localStorage.mytubeFinder_search_term
      else
        that.collection = new VideosCollection []
      that.collection.fetch
        success: onDataHandler
        dataType: 'jsonp'

    search: (e) ->
      e.preventDefault()
      that = this

      search_term = $('form input').val()
      if search_term
        localStorage.mytubeFinder_search_term = search_term
        that.initialize()

    render: ->
      total_videos = @collection.models.length

      data = 
        total_videos: total_videos

      if localStorage.mytubeFinder_search_term?
        data.search_term = localStorage.mytubeFinder_search_term
      else
        data.search_term = ''

      compiledTemplate = _.template videosTemplate, data
      @$el.html compiledTemplate

      videoListView = new VideosListView
        collection: @collection
      videoListView.render()

  HomeView