define [
  'jquery'
  'underscore'
  'backbone'
  'collections/videos/VideosCollection'
  'text!templates/videos/videosListTemplate.html'
], ($, _, Backbone, VideosCollection, videosListTemplate) ->

  VideosListView = Backbone.View.extend

    videos: []

    el: $('#video-list')

    initialize: ->
      that = this
      that.bind 'reset', that.clearView

    resetVideos: ->
      @videos = []

    render: ->
      @resetVideos()

      @createDataObject @collection.models

      data = 
        videos: @videos

      videosCompiledTemplate = _.template videosListTemplate, data
      $('#video-list').html videosCompiledTemplate

    createDataObject: (models) ->
      that = this
      _.each models, (video) ->
        that.videos.push
          title: video.get 'title'
          author: video.get 'author'
          link: video.get 'link'
          thumbnail: video.get 'thumbnail'
          duration: video.get 'duration'
          uploaded: (new Date(video.get 'uploaded')).toLocaleString()
          views: that.formatNumber video.get 'views'

    formatNumber: (x) ->
      return 0 if !x
      x.toString().replace /\B(?=(\d{3})+(?!\d))/g, ","

  VideosListView