define [
  'jquery'
  'underscore'
  'backbone'
], ($, _, Backbone) ->

  VideoModel = Backbone.Model.extend

    defaults:
      title: 'jp title'

  VideoModel