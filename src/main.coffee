# Author: JP Atiaga <jpatiaga@gmail.com>
# Filename: main.js

require.config 
  shim:
    underscore:
      exports: '_'
    backbone:
      deps: ['underscore', 'jquery']
      exports: 'Backbone'
  paths:
    jquery: '//code.jquery.com/jquery'
    bootstrap: '//netdna.bootstrapcdn.com/bootstrap/3.0.1/js/bootstrap.min'
    underscore: '//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min'
    backbone: '//cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.0/backbone-min'
    templates: '../templates'

require ['app'], (App) ->
  App.initialize()