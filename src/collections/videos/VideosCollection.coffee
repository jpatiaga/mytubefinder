define [
  'jquery'
  'underscore'
  'backbone'
  'models/videos/VideosModel'
], ($, _, Backbone, VideosModel) ->

  VideosCollection = Backbone.Collection.extend

    model: VideosModel

    search_term: null

    initialize: (models, options) ->
      if options?
        @search_term = options.search_term

    url: ->
      if @search_term
        url = "http://gdata.youtube.com/feeds/api/videos?q=#{@search_term}&alt=json-in-script&max-results=30&format=5"
      else
        url = 'http://gdata.youtube.com/feeds/api/standardfeeds/most_popular?v=2&alt=json'

    parse: (data) ->
      videos = []
      _.each data.feed.entry, (video) ->
        _video = {}
        _video.title = video.title.$t if video.title?
        _video.author = video.author[0].name.$t if video.author[0]?
        _video.link = video.link[0].href if video.link[0]?
        _video.thumbnail = video.media$group.media$thumbnail[0].url if video.media$group.media$thumbnail[0]?
        _video.duration = video.media$group.yt$duration.seconds if video.media$group.yt$duration?
        _video.uploaded = video.media$group.yt$uploaded.$t if video.media$group.yt$uploaded?
        _video.uploaded = video.updated.$t if !video.media$group.yt$uploaded? and video.updated?
        _video.views = video.yt$statistics.viewCount if video.yt$statistics?
        videos.push _video
      videos

  VideosCollection