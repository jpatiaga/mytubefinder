define [
  'jquery'
  'underscore'
  'backbone'
  'views/home/HomeView'
], ($, _, Backbone, HomeView) ->
  
  AppRouter = Backbone.Router.extend
    routes:
      '*actions' : 'defaultAction'

  initialize = ->
    app_router = new AppRouter

    app_router.on 'route:defaultAction', (actions) ->
      window.homeView = new HomeView()
      window.homeView.render()

    Backbone.history.start()

  initialize: initialize