// Generated by CoffeeScript 1.4.0
(function() {

  define(['jquery', 'underscore', 'backbone', 'collections/videos/VideosCollection', 'text!templates/videos/videosListTemplate.html'], function($, _, Backbone, VideosCollection, videosListTemplate) {
    var VideosListView;
    VideosListView = Backbone.View.extend({
      videos: [],
      el: $('#video-list'),
      initialize: function() {
        var that;
        that = this;
        return that.bind('reset', that.clearView);
      },
      resetVideos: function() {
        return this.videos = [];
      },
      render: function() {
        var data, videosCompiledTemplate;
        this.resetVideos();
        this.createDataObject(this.collection.models);
        data = {
          videos: this.videos
        };
        videosCompiledTemplate = _.template(videosListTemplate, data);
        return $('#video-list').html(videosCompiledTemplate);
      },
      createDataObject: function(models) {
        var that;
        that = this;
        return _.each(models, function(video) {
          return that.videos.push({
            title: video.get('title'),
            author: video.get('author'),
            link: video.get('link'),
            thumbnail: video.get('thumbnail'),
            duration: video.get('duration'),
            uploaded: (new Date(video.get('uploaded'))).toLocaleString(),
            views: that.formatNumber(video.get('views'))
          });
        });
      },
      formatNumber: function(x) {
        if (!x) {
          return 0;
        }
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
    });
    return VideosListView;
  });

}).call(this);
