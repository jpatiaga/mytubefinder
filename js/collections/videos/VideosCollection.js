// Generated by CoffeeScript 1.4.0
(function() {

  define(['jquery', 'underscore', 'backbone', 'models/videos/VideosModel'], function($, _, Backbone, VideosModel) {
    var VideosCollection;
    VideosCollection = Backbone.Collection.extend({
      model: VideosModel,
      search_term: null,
      initialize: function(models, options) {
        if (options != null) {
          return this.search_term = options.search_term;
        }
      },
      url: function() {
        var url;
        if (this.search_term) {
          return url = "http://gdata.youtube.com/feeds/api/videos?q=" + this.search_term + "&alt=json-in-script&max-results=30&format=5";
        } else {
          return url = 'http://gdata.youtube.com/feeds/api/standardfeeds/most_popular?v=2&alt=json';
        }
      },
      parse: function(data) {
        var videos;
        videos = [];
        _.each(data.feed.entry, function(video) {
          var _video;
          _video = {};
          if (video.title != null) {
            _video.title = video.title.$t;
          }
          if (video.author[0] != null) {
            _video.author = video.author[0].name.$t;
          }
          if (video.link[0] != null) {
            _video.link = video.link[0].href;
          }
          if (video.media$group.media$thumbnail[0] != null) {
            _video.thumbnail = video.media$group.media$thumbnail[0].url;
          }
          if (video.media$group.yt$duration != null) {
            _video.duration = video.media$group.yt$duration.seconds;
          }
          if (video.media$group.yt$uploaded != null) {
            _video.uploaded = video.media$group.yt$uploaded.$t;
          }
          if (!(video.media$group.yt$uploaded != null) && (video.updated != null)) {
            _video.uploaded = video.updated.$t;
          }
          if (video.yt$statistics != null) {
            _video.views = video.yt$statistics.viewCount;
          }
          return videos.push(_video);
        });
        return videos;
      }
    });
    return VideosCollection;
  });

}).call(this);
