// Generated by CoffeeScript 1.4.0
(function() {

  define(['jquery', 'underscore', 'backbone', 'models/video/VideoModel'], function($, _, Backbone, VideoModel) {
    var VideoCollection;
    VideoCollection = Backbone.Collection.extend({
      model: VideoModel,
      initialize: function(models, options) {},
      url: function() {
        return 'http://gdata.youtube.com/feeds/api/standardfeeds/most_popular?v=2&alt=json';
      },
      parse: function(data) {
        return data.feed.entry;
      }
    });
    return VideoCollection;
  });

}).call(this);
